import React from 'react';
import { useEffect } from 'react';
import { Animated, Image, Text } from 'react-native';
import { ILLogo } from '../../assets';
import { BoxContainer } from '../../components';
import { getSymptoms,  getVaccination } from '../../services';
import { getData, removeData } from '../../utils';
import styles from './styles';

const Splash: React.FC<any> = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      _handleGetData();
    }, 3000);
  }, []);
  const _handleGetData = async () => {
    await getVaccination();
    await getSymptoms();
    navigation.replace('Dashboard');
  };
  return (
    <BoxContainer style={{ ...styles.container }}>
      <Image style={styles.imageAppLogo} source={ILLogo} />
      <Animated.Text style={styles.textAppName}>SIKARMA</Animated.Text>
      <Text style={styles.textAppDesc}>SISTEM PAKAR DETEKSI ASMA</Text>
    </BoxContainer>
  );
};

export default Splash;
