import React, { useCallback, useState } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { ILLogo } from '../../assets';
import CheckBox from '@react-native-community/checkbox';
import { BoxContainer, Button, Gap, Header } from '../../components';
import { color, FONT_LIGHT, FONT_MEDIUM, FONT_REGULAR } from '../../theme';
import { TextInput } from 'react-native-gesture-handler';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import base64 from 'react-native-base64'
import { postSelfAssessment } from '../../services/assessment';
import { useEffect } from 'react';
import { RootStateOrAny, useSelector } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import { createRef } from 'react';
import showMessage from '../../utils/showMessage';
import { BottomTabBar } from '@react-navigation/bottom-tabs';

const SelfAssessment: React.FC<any> = ({ navigation }) => {
  const [keparahan, setKeparahan] = useState<Array<any>>([]);
  const [name, setName] = useState('');
  const { isLoading } = useSelector(
    (state: RootStateOrAny) => state.globalReducer,
  );
  const { symptoms, symptom } = useSelector(
    (state: RootStateOrAny) => state.symptomReducer,
  );
  const nameRef = createRef<any>();
  var radio_props = [
    { label: 'Ya', value: 'ya' },
    { label: 'Tidak', value: 'tidak' }
  ];

  const _handleAssessment = async () => {
    if (name === '' || name === undefined){
      showMessage('Nama harus diisi!', 'error');
      return nameRef.current.focus();
    } 

    if (keparahan.length - 1 !== symptoms.length){
      showMessage('Semua gejala harus diisi', 'error');
      return;
    }

    let object: any = {};
    const params:any = [];
    const formData = new FormData();
    keparahan?.forEach((item, index) => {
      // params[index] = item;
      if(item === undefined) return;

      return (object[`gejala[${index}]`] = item);
    });


    // object['gejala'] = params;
    object['nama'] = name;
    formData.append('nama', name);
    await postSelfAssessment(object);
    // console.log(base64.encode(JSON.stringify(params)));
    setTimeout(() => {
      if (!isLoading) {
        navigation.navigate('ResultAssessment');
      }
    }, 1500);
  };

  const _onUpdate = (index: any, value: any) => {
    let newArr = keparahan; // copying the old datas array
    // let newArr = [];
    newArr[index] = value; // replace e.target.value with whatever you want to change it to
    setKeparahan([...newArr]); // ??
  };

  return (
    <BoxContainer>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Header
            title="SIKARMA"
            subTitle="SISTEM PAKAR DETEKSI ASMA"
            style={styles.header}
            back
            onPress={() => navigation.goBack()}
          />
        </View>
        <View style={{ flex: 1, paddingTop: 10 }}>
          <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center' }}>
            Self Assesment
          </Text>

          <Gap height={10} />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ padding: 5 }}>
              <Text style={{ ...FONT_REGULAR(14), width: '90%' }}>
                Siapa nama anda?
              </Text>
              <View>
                <TextInput
                  style={{
                    borderWidth: 2,
                    borderRadius: 5,
                    textAlignVertical: 'center',
                    color: color.primary,
                    paddingHorizontal: 10,
                  }}
                  value={name}
                  onChangeText={(value) => setName(value)}
                  ref={nameRef}
                />
              </View>
            </View>
            {symptoms.length > 0 &&
              symptoms !== undefined &&
              symptoms?.map((item: any, index: any) => (
                <View style={{ padding: 5 }} key={index?.toString()}>
                  <Text style={{ ...FONT_REGULAR(14), width: '90%' }}>
                    {item?.nama} ?
                  </Text>
                  <View>
                    <RadioForm
                      animation={true}
                      formHorizontal={true}
                      labelHorizontal={true}
                      initial={0}
                    >
                      {radio_props.map((obj, i) => {
                        return (
                          <RadioButton labelHorizontal={false} key={i}>
                            <RadioButtonInput
                              obj={obj}
                              index={i}
                              isSelected={keparahan[item?.id] == obj?.value}
                              onPress={(thisValue: any) =>
                                _onUpdate(item?.id, thisValue)
                              }
                              borderWidth={2}
                              buttonInnerColor={'#10536D'}
                              buttonOuterColor={
                                keparahan[item?.id] == obj?.value
                                  ? '#10536D'
                                  : '#6F8B9A'
                              }
                              buttonSize={10}
                              buttonOuterSize={20}
                              buttonStyle={{}}
                              buttonWrapStyle={{ marginLeft: 10 }}
                            />
                            <RadioButtonLabel
                              obj={obj}
                              index={i}
                              labelHorizontal={true}
                              onPress={(thisValue: any) =>
                                _onUpdate(item?.id, thisValue)
                              }
                              labelStyle={{
                                ...FONT_MEDIUM(14),
                                color:
                                  keparahan[item?.id] == obj?.value
                                    ? '#10536D'
                                    : '#6F8B9A',
                              }}
                              labelWrapStyle={{}}
                            />
                          </RadioButton>
                        );
                      })}
                    </RadioForm>
                  </View>
                </View>
              ))}

            <Gap height={10} />
            <Button onPress={_handleAssessment}>
              {isLoading ? (
                <ActivityIndicator size="large" color={color.white} />
              ) : (
                'Kirim'
              )}
            </Button>
            <Gap height={20} />
          </ScrollView>
        </View>
      </View>
    </BoxContainer>
  );
};

export default SelfAssessment;

const styles = StyleSheet.create({
  header: { paddingHorizontal: 10, paddingTop: 15 },
  container: {
    flex: 1,
    backgroundColor: color.white,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    paddingHorizontal: 10,
  },
});
