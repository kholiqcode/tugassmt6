import React from 'react';
import { useEffect } from 'react';
import { memo } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { ILLogo } from '../../assets';
import { BoxContainer, Gap, Header, Button } from '../../components';
import { setAssessment, setVaccination } from '../../libs';
import { color, FONT_MEDIUM, FONT_REGULAR } from '../../theme';

const ResultAssessment: React.FC<any> = ({ navigation }) => {
  const { assessment } = useSelector(
    (state: RootStateOrAny) => state.assessmentReducer,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      dispatch(setAssessment(null));
    };
  }, []);
  return (
    <BoxContainer>
      <ScrollView
        style={{ paddingHorizontal: 10 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ flex: 1 }}>
          <Header
            title="SIKARMA"
            subTitle="SISTEM PAKAR DETEKSI ASMA"
            style={{ paddingHorizontal: 10, paddingTop: 15 }}
            back
            onPress={() => navigation.goBack()}
          />
          <Gap height={20} />
        </View>
        <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center' }}>
          Hasil Self Assesment
        </Text>
        <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center',textDecorationLine:'underline' }}>
          {assessment?.nama ?? ''}
        </Text>
        <Gap
          height={10}
          style={{ borderBottomWidth: 1, borderBottomColor: '#EEEE' }}
        />
        <View style={{ flex: 1 }}>
          <Gap height={20} />
          <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center' }}>
            Akut : {assessment?.akut ?? 0}%
          </Text>
          <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center' }}>
            Kronis : {assessment?.kronis ?? 0}%
          </Text>
          <Text style={{ ...FONT_MEDIUM(24), textAlign: 'center' }}>
            Periodik : {assessment?.periodik ?? 0}%
          </Text>
          <Gap
            height={20}
            style={{ borderBottomWidth: 1, borderBottomColor: '#EEEE' }}
          />
          {assessment?.kondisi && (
            <View>
              <Gap height={20} />
              <Text style={{ ...FONT_MEDIUM(14), textAlign: 'center' }}>
                Kondisi Sekarang
              </Text>
              {assessment?.kondisi?.map((item: any, index: any) => {
                return (
                  <View key={index.toString()}>
                    <Gap height={10} />
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <View
                        style={{
                          backgroundColor: '#495A75',
                          width: 15,
                          height: 15,
                          borderRadius: 30,
                        }}
                      />
                      <Gap width={15} />
                      <Text style={{ ...FONT_REGULAR(14), flexShrink: 1 }}>
                        {item?.nama} {'=>'} {item?.keparahan}
                      </Text>
                    </View>
                  </View>
                );
              })}
            </View>
          )}
          <Gap height={20} />
          <Text style={{ ...FONT_MEDIUM(14), textAlign: 'center' }}>
            Saran Untuk Pemulihan ASMA
          </Text>
          <Gap height={10} />
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View
                style={{
                  backgroundColor: '#495A75',
                  width: 15,
                  height: 15,
                }}
              />
              <Gap width={15} />
              <Text style={{ ...FONT_REGULAR(14), flexShrink: 1 }}>
                Hindari faktor pemicu kekambuhan asma, seperti asap rokok,
                polusi udara, bahan kimia, dll.
              </Text>
            </View>
          </View>
          <Gap height={10} />
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View
                style={{
                  backgroundColor: '#495A75',
                  width: 15,
                  height: 15,
                }}
              />
              <Gap width={15} />
              <Text style={{ ...FONT_REGULAR(14), flexShrink: 1 }}>
                Rutin cek fungsi paru-paru dengan peak flow meter.
              </Text>
            </View>
          </View>
          <Gap height={10} />
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View
                style={{
                  backgroundColor: '#495A75',
                  width: 15,
                  height: 15,
                }}
              />
              <Gap width={15} />
              <Text style={{ ...FONT_REGULAR(14), flexShrink: 1 }}>
                Minum obat sesuai yang dianjurkan dokter
              </Text>
            </View>
          </View>
        </View>
        <Gap height={20} />
        <Button onPress={() => navigation.navigate('Dashboard')}>
          Beranda
        </Button>
        <Gap height={20} />
      </ScrollView>
    </BoxContainer>
  );
};

export default memo(ResultAssessment);

const styles = StyleSheet.create({});
