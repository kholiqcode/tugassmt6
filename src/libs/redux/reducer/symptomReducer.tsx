const inititalState = {
  symptom: [],
  symptoms: [],
};

export const symptomReducer = (state = inititalState, action: any) => {
  switch (action.type) {
    case 'SET_SYMPTOM':
      return {
        ...state,
        symptom: action.value,
      };
    case 'SET_SYMPTOMS':
      return {
        ...state,
        symptoms: action.value,
      };

    default:
      return {
        ...state,
      };
  }
};
