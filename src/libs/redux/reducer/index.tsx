import { combineReducers } from 'redux';
import { globalReducer } from './globalReducer';
import { vaccinationReducer } from './vaccinationReducer';
import { hospitalReducer } from './hospitalReducer';
import { assessmentReducer } from './assessmentReducer';
import { symptomReducer } from './symptomReducer';

const reducer = combineReducers({
  globalReducer,
  vaccinationReducer,
  hospitalReducer,
  assessmentReducer,
  symptomReducer,
});

export default reducer;